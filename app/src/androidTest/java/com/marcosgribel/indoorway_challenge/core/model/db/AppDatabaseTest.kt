package com.marcosgribel.indoorway_challenge.core.model.db

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.persistence.room.Room
import android.support.test.InstrumentationRegistry
import com.marcosgribel.indoorway_challenge.core.model.domain.entity.Data
import io.reactivex.Completable
import org.junit.Before
import org.junit.Rule
import org.junit.Test

/**
 * Created by marcosgribel on 7/6/18.
 *
 *
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 */
class AppDatabaseTest {

    @get:Rule
    open var instantTaskExecutor = InstantTaskExecutorRule()

    private lateinit var appDatabase: AppDatabase

    private val dataGoolge = Data("https://www.google.com/", "Google")
    private val dataIndoorway = Data("https://www.indoorway.com/", "Indoorway")


    @Before
    fun setUp() {
        var targetContext = InstrumentationRegistry.getTargetContext()
        val context = InstrumentationRegistry.getContext()

        appDatabase = Room.inMemoryDatabaseBuilder(targetContext,
                AppDatabase::class.java)
                .allowMainThreadQueries()
                .build()

    }


    @Test
    fun test_insertData() {

        val dataDao = appDatabase.dataDao()

        Completable.fromAction {
            dataDao.insert(*arrayOf(dataGoolge, dataIndoorway))
        }.test().assertComplete()

        dataDao.getByUrl(dataGoolge.url)
                .test().assertValue {
                    it == dataGoolge
                }


    }

}