package com.marcosgribel.indoorway_challenge.main.ui

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import com.marcosgribel.indoorway_challenge.R
import com.marcosgribel.indoorway_challenge.core.model.domain.enumerator.Status
import com.marcosgribel.indoorway_challenge.core.ui.viewmodel.InternetConnectivityManager
import com.marcosgribel.indoorway_challenge.data.ui.DataDetailActivity
import com.marcosgribel.indoorway_challenge.main.viewmodel.MainViewModel
import com.marcosgribel.indoorway_challenge.webview.ui.WebViewContentActivity
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.android.inject
import timber.log.Timber

class MainActivity : AppCompatActivity() {

    init {
        val TAG = MainActivity::class.java.simpleName.toString()
        Timber.tag(TAG)
    }

    private val internetConnectivity: InternetConnectivityManager by inject()
    private val mainViewModel: MainViewModel by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initialize()
    }


    fun initialize() {

        btn_download_with_webview.setOnClickListener(onDownloadWithWebViewClickListener())
        btn_download_without_webview.setOnClickListener(onDownloadWithoutWebViewClickListener())


        internetConnectivity.observe(this, Observer {
            if (!it!!)
                onOffline()
        })

        mainViewModel.status.observe(this, handleStatusObserver())

    }


    fun onOffline() {
        Toast.makeText(this, getString(R.string.msg_you_are_offilne), Toast.LENGTH_SHORT).show()
    }


    private fun onDownloadWithWebViewClickListener(): View.OnClickListener {
        return View.OnClickListener {
            val url = edit_url.text.toString()
            if(mainViewModel.validate(url))
                startActivity(WebViewContentActivity.newIntent(this, url))
        }

    }

    private fun onDownloadWithoutWebViewClickListener(): View.OnClickListener {
        return View.OnClickListener {
            val url = edit_url.text.toString()
            if(mainViewModel.validate(url))
                startActivity(DataDetailActivity.newIntent(this, url))
        }

    }

    private fun handleStatusObserver(): Observer<Status> {
        return Observer { status ->
            when (status) {
                Status.ERROR -> handleError()
            }
        }
    }

    fun handleError(){
        edit_url.setError(getString(R.string.msg_can_not_be_null_or_empty))
    }


}
