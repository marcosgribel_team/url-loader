package com.marcosgribel.indoorway_challenge.main.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.marcosgribel.indoorway_challenge.core.model.domain.enumerator.Status

/**
 * Created by marcosgribel on 7/8/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
class MainViewModel: ViewModel() {

    var status = MutableLiveData<Status>()

    fun validate(str : String) : Boolean {
        if( str.isNullOrEmpty() || str.isNullOrBlank() ){
            status.value = Status.ERROR
            return false
        }
            return true

    }

}