package com.marcosgribel.indoorway_challenge.webview.ui

import android.arch.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.webkit.WebChromeClient
import android.webkit.WebSettings
import android.webkit.WebView
import android.widget.Toast
import com.marcosgribel.indoorway_challenge.R
import com.marcosgribel.indoorway_challenge.core.ui.viewmodel.InternetConnectivityManager
import kotlinx.android.synthetic.main.activity_web_content.*
import org.koin.android.ext.android.inject


class WebViewContentActivity : AppCompatActivity() {

    companion object {

        fun newIntent(context: Context, url: String): Intent {
            var intent = Intent(context, WebViewContentActivity::class.java)
            intent.putExtra(context.getString(R.string.args_url), url)
            return intent
        }

    }


    private val internetConnectivity: InternetConnectivityManager by inject()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web_content)


        val url = intent.extras.getString(getString(R.string.args_url))
        initialize(url)
    }


    fun initialize(url: String) {

        web_view.settings.setAppCachePath(applicationContext.cacheDir.absolutePath)
        web_view.settings.setAppCacheEnabled(true)
        web_view.settings.allowFileAccess = true
        web_view.settings.javaScriptEnabled = true
        web_view.settings.cacheMode = WebSettings.LOAD_DEFAULT

        web_view.webChromeClient = onWebChromeClient()
        web_view.loadUrl(url)


        internetConnectivity.observe(this, Observer {
            if (it!!)
                onOnline()
            else
                onOffline()
        })

    }


    fun onOnline() {
        web_view.settings.cacheMode = WebSettings.LOAD_DEFAULT
    }

    fun onOffline() {
        web_view.settings.cacheMode = WebSettings.LOAD_CACHE_ELSE_NETWORK
        Toast.makeText(this, getString(R.string.msg_you_are_offilne), Toast.LENGTH_SHORT).show()
    }

    private fun onWebChromeClient(): WebChromeClient {
        return object : WebChromeClient() {
            override fun onProgressChanged(view: WebView?, newProgress: Int) {
                super.onProgressChanged(view, newProgress)
                progress_bar.progress = newProgress

                if (newProgress == 100)
                    onLoadContentSuccess()
            }
        }


    }


    fun onLoadContentSuccess() {
        web_view.visibility = View.VISIBLE
        progress_bar.visibility = View.GONE
    }


}


