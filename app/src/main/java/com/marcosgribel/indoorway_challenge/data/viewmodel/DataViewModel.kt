package com.marcosgribel.indoorway_challenge.data.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.marcosgribel.indoorway_challenge.core.model.domain.entity.Data
import com.marcosgribel.indoorway_challenge.core.model.domain.enumerator.Status
import com.marcosgribel.indoorway_challenge.core.model.usecase.DataUseCase
import com.marcosgribel.indoorway_challenge.core.rx.ScheduleProvider
import io.reactivex.disposables.CompositeDisposable
import timber.log.Timber

/**
 * Created by marcosgribel on 7/6/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
class DataViewModel constructor(private val useCase: DataUseCase, private val scheduler: ScheduleProvider) : ViewModel() {

    init {
        Timber.tag(DataViewModel::class.java.simpleName.toString())
    }

    var compositeDisposable = CompositeDisposable()
    var cacheStrategy = MutableLiveData<Int>()
    var data = MutableLiveData<Data>()
    var status = MutableLiveData<Status>()



    fun loadData(url: String) {

        val disposable = useCase.getDataFromUrl(cacheStrategy.value!!, url)
                .subscribeOn(scheduler.io())
                .observeOn(scheduler.mainThread())
                .doOnSubscribe { status.value = Status.LOADING }
                .subscribe({
                    status.value = Status.SUCCESS
                    data.value = it
                }, {
                    status.value = Status.ERROR
                    Timber.e(it)
                })

        compositeDisposable.add(disposable)
    }

    override fun onCleared() {
        compositeDisposable.clear()
        super.onCleared()
    }
}