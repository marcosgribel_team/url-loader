package com.marcosgribel.indoorway_challenge.data.ui

import android.arch.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.marcosgribel.indoorway_challenge.R
import com.marcosgribel.indoorway_challenge.core.model.domain.entity.Data
import com.marcosgribel.indoorway_challenge.core.model.domain.enumerator.Status
import com.marcosgribel.indoorway_challenge.core.model.service.BaseService
import com.marcosgribel.indoorway_challenge.core.ui.viewmodel.InternetConnectivityManager
import com.marcosgribel.indoorway_challenge.data.viewmodel.DataViewModel
import kotlinx.android.synthetic.main.activity_data_content.*
import org.koin.android.ext.android.inject
import timber.log.Timber

class DataDetailActivity : AppCompatActivity() {


    init {
        val TAG = DataDetailActivity::class.java.simpleName.toString()
        Timber.tag(TAG)
    }


    private val internetConnectivity: InternetConnectivityManager by inject()
    private val dataViewModel: DataViewModel by inject()



    companion object {

        fun newIntent(context: Context, url: String): Intent {
            var intent = Intent(context, DataDetailActivity::class.java)
            intent.putExtra(context.getString(R.string.args_url), url)
            return intent
        }

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_data_content)

        dataViewModel.cacheStrategy.value = BaseService.NETWORK_FIRST_STRATEGY

        internetConnectivity.observe(this, handleInternetObserver())

        dataViewModel.data.observe(this, handleDataObserver())
        dataViewModel.status.observe(this, handleStatusObserver())

        val url = intent.extras.getString(getString(R.string.args_url))
        loadData(url)

    }

    fun loadData(url: String){
        dataViewModel.loadData(url)
    }


    private fun handleInternetObserver(): Observer<Boolean> {
        return Observer<Boolean> {
            if(it!!) {
                dataViewModel.cacheStrategy.value = BaseService.NETWORK_FIRST_STRATEGY
            }else {
                dataViewModel.cacheStrategy.value = BaseService.CACHE_FIRST_STRATEGY
            }

        }
    }

    private fun handleDataObserver(): Observer<Data> {
        return Observer<Data> { data -> setData(data) }
    }

    fun setData(data: Data?) {
        txt_data_detail.text = data?.content
    }


    private fun handleStatusObserver(): Observer<Status> {
        return Observer { status ->
            when (status) {
                Status.LOADING -> handleLoading()
                Status.SUCCESS -> handleSuccess()
                Status.ERROR -> handleError()
            }
        }
    }

    fun handleError() {
        progress_bar.visibility = View.GONE
        frame_data_detail.visibility = View.GONE
        frame_error_404.visibility = View.VISIBLE
    }

    fun handleLoading() {
        progress_bar.visibility = View.VISIBLE
        frame_data_detail.visibility = View.GONE
        frame_error_404.visibility = View.GONE
    }

    fun handleSuccess() {
        progress_bar.visibility = View.GONE
        frame_data_detail.visibility = View.VISIBLE
        frame_error_404.visibility = View.GONE
    }
}
