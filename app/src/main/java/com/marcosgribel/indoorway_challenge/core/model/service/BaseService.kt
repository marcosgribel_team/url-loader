package com.marcosgribel.indoorway_challenge.core.model.service

import android.support.annotation.IntDef
import com.marcosgribel.indoorway_challenge.core.di.RetrofitModule
import io.reactivex.Single
import java.io.IOException

/**
 * Created by marcosgribel on 7/6/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */


class BaseService constructor(private val retrofit: RetrofitModule) {

    companion object {
        const val NETWORK_FIRST_STRATEGY = 0
        const val CACHE_FIRST_STRATEGY = 1

        @Retention(AnnotationRetention.SOURCE)
        @IntDef(NETWORK_FIRST_STRATEGY, CACHE_FIRST_STRATEGY)
        annotation class CacheStrategy

    }

    fun get(url: String, clazz: Class<DataService>): DataService? {
        return retrofit.connect(url).create(clazz)
    }


    fun <T> getNetworkCacheSingle(@CacheStrategy cacheStrategy: Int = NETWORK_FIRST_STRATEGY,
                                  networkObservable: Single<T>,
                                  cacheObservable: Single<T>
    ): Single<T> {

        return if (cacheStrategy == NETWORK_FIRST_STRATEGY) {
            networkObservable
                    .onErrorResumeNext { t: Throwable ->
                            if (t is IOException)
                                return@onErrorResumeNext getNetworkCacheSingle(CACHE_FIRST_STRATEGY, networkObservable, cacheObservable)
                            return@onErrorResumeNext Single.error(t)

                    }
        } else {
            cacheObservable
        }
    }

}