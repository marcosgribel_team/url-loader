package com.marcosgribel.indoorway_challenge.core.di

import android.arch.persistence.room.Room
import android.content.Context
import com.marcosgribel.indoorway_challenge.R
import com.marcosgribel.indoorway_challenge.core.model.db.AppDatabase

/**
 * Created by marcosgribel on 7/6/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
class DatabaseModule () {

    fun build(context: Context) : AppDatabase{
        return Room.databaseBuilder(context,
                AppDatabase::class.java,
                context.getString(R.string.app_database_name))
                .fallbackToDestructiveMigration()
                .build()
    }

}