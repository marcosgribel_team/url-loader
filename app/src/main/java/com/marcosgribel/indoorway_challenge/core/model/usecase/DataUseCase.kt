package com.marcosgribel.indoorway_challenge.core.model.usecase

import com.marcosgribel.indoorway_challenge.core.model.domain.entity.Data
import com.marcosgribel.indoorway_challenge.core.model.service.DataRepository
import io.reactivex.Single

/**
 * Created by marcosgribel on 7/4/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */


interface DataUseCase {

    fun getDataFromUrl(cacheStrategy: Int, url: String): Single<Data>

}

class DataUseCaseImpl constructor(private val repository: DataRepository) : DataUseCase {

    override fun getDataFromUrl(cacheStrategy: Int, url: String): Single<Data> {
        return repository
                .getData(cacheStrategy, url)
                .map {
                    repository.insert(it)
                    it
                }

    }

}