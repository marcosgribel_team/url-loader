package com.marcosgribel.indoorway_challenge.core.model.domain.enumerator

/**
 * Created by marcosgribel on 7/6/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
enum class Status {
    LOADING,
    SUCCESS,
    ERROR
}