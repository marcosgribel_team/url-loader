package com.marcosgribel.indoorway_challenge.core.model.db

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.marcosgribel.indoorway_challenge.core.model.domain.entity.Data
import io.reactivex.Single

/**
 * Created by marcosgribel on 7/6/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
@Dao
interface DataDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(data: Data)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg data: Data)

    @Query("SELECT * FROM Data WHERE url = :url")
    fun getByUrl(url: String) : Single<Data>

}