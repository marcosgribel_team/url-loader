package com.marcosgribel.indoorway_challenge.core.model.service

import io.reactivex.Single
import okhttp3.HttpUrl
import okhttp3.ResponseBody
import retrofit2.http.GET
import retrofit2.http.Url

/**
 * Created by marcosgribel on 7/6/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
interface DataService {

    @GET
    fun getDataFromUrl(@Url url : HttpUrl) : Single<ResponseBody>

}