package com.marcosgribel.indoorway_challenge.core.di

import android.content.Context
import com.marcosgribel.indoorway_challenge.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import java.util.concurrent.TimeUnit

/**
 * Created by marcosgribel on 7/4/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
class RetrofitModule constructor(context: Context) {


    internal fun connect(apiBaseUrl: String): Retrofit {

        return Retrofit.Builder()
                .baseUrl(apiBaseUrl)
                .addCallAdapterFactory(provideRxJava2CallAdapterFactory())
                .client(provideOkHttpClient())
                .build()
    }

    private fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor {
        var httpLoggingInterceptorLevel = HttpLoggingInterceptor.Level.NONE
        if (BuildConfig.DEBUG)
            httpLoggingInterceptorLevel = HttpLoggingInterceptor.Level.BODY

        return HttpLoggingInterceptor().apply {
            level = httpLoggingInterceptorLevel
        }
    }


    private fun provideOkHttpClient(): OkHttpClient {
        return OkHttpClient()
                .newBuilder()
                .addInterceptor(provideHttpLoggingInterceptor())
                .readTimeout(45, TimeUnit.SECONDS)
                .connectTimeout(2, TimeUnit.SECONDS)
                .build()
    }


    private fun provideRxJava2CallAdapterFactory(): RxJava2CallAdapterFactory = RxJava2CallAdapterFactory.create()
}