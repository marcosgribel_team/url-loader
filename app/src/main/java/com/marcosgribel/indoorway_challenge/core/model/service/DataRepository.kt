package com.marcosgribel.indoorway_challenge.core.model.service

import com.marcosgribel.indoorway_challenge.core.exception.CacheEmptyException
import com.marcosgribel.indoorway_challenge.core.model.db.DataDao
import com.marcosgribel.indoorway_challenge.core.model.domain.entity.Data
import io.reactivex.Completable
import io.reactivex.Single
import okhttp3.HttpUrl

/**
 * Created by marcosgribel on 7/6/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
interface DataRepository {

    fun insert(data: Data): Completable

    fun getData(@BaseService.Companion.CacheStrategy cacheStrategy: Int, url: String): Single<Data>

}

class DataRepositoryImpl constructor(val service: BaseService, val dao: DataDao) : DataRepository {


    override fun insert(data: Data): Completable {
        return Completable.fromAction {
            dao.insert(data)
        }
    }

    override fun getData(cacheStrategy: Int, url: String): Single<Data> {
        val network = getDataOnline(url)
        val cache = getDataOffline(url)
        return service.getNetworkCacheSingle(cacheStrategy, network, cache)
    }

    private fun getDataOffline(url: String): Single<Data> {
        return dao.getByUrl(url).flatMap {
            if (it == null)
                throw CacheEmptyException()

            Single.just(it)
        }
    }

    private fun getDataOnline(url: String): Single<Data> {
        val response = service.get(url, DataService::class.java)!!.getDataFromUrl(HttpUrl.parse(url)!!)
        return response.flatMap {
            val data = Data(url, it.string())
            Single.just(data)
        }
    }


}
