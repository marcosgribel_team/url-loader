package com.marcosgribel.indoorway_challenge.core.di

import com.marcosgribel.indoorway_challenge.core.model.db.AppDatabase
import com.marcosgribel.indoorway_challenge.core.model.service.BaseService
import com.marcosgribel.indoorway_challenge.core.model.service.DataRepository
import com.marcosgribel.indoorway_challenge.core.model.service.DataRepositoryImpl
import com.marcosgribel.indoorway_challenge.core.model.usecase.DataUseCase
import com.marcosgribel.indoorway_challenge.core.model.usecase.DataUseCaseImpl
import com.marcosgribel.indoorway_challenge.core.rx.AppScheduler
import com.marcosgribel.indoorway_challenge.core.rx.ScheduleProvider
import com.marcosgribel.indoorway_challenge.core.ui.viewmodel.InternetConnectivityManager
import com.marcosgribel.indoorway_challenge.data.viewmodel.DataViewModel
import com.marcosgribel.indoorway_challenge.main.viewmodel.MainViewModel
import org.koin.android.architecture.ext.viewModel
import org.koin.dsl.module.applicationContext

/**
 * Created by marcosgribel on 6/29/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */

val coreModule = applicationContext {
    bean { RetrofitModule(get()) }
    bean { BaseService(get()) }
    bean { InternetConnectivityManager(get()) }
}

val dbModule = applicationContext {
    bean { DatabaseModule().build(get()) }
    bean { get<AppDatabase>().dataDao() }
}

val mainModule = applicationContext {
    bean { MainViewModel() }
}

val dataModule = applicationContext {

    bean { DataRepositoryImpl(get(), get()) as DataRepository }
    bean { DataUseCaseImpl(get()) as DataUseCase }
    viewModel { DataViewModel(get(), get()) }

}

val rxModule = applicationContext {
    bean { AppScheduler() as ScheduleProvider }
}


val appModule = listOf(coreModule, dbModule, rxModule, mainModule, dataModule)
