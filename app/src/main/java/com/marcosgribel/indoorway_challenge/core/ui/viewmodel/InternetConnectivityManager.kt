package com.marcosgribel.indoorway_challenge.core.ui.viewmodel

import android.arch.lifecycle.LiveData
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.NetworkInfo

/**
 * Created by marcosgribel on 6/29/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
open class InternetConnectivityManager constructor(private val application: Context) : LiveData<Boolean>() {

    private var broadcastReceiver: BroadcastReceiver? = null


    override fun onActive() {
        super.onActive()
        registerBroadCast()
    }

    override fun onInactive() {
        super.onInactive()
        unRegisterBroadCast()
    }


    private fun registerBroadCast() {
        if (broadcastReceiver != null) return

        broadcastReceiver.let {
            val intentFilter = IntentFilter()
            intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION)
            application.registerReceiver(onBroadCastReceiver(), intentFilter)
        }

    }

    private fun unRegisterBroadCast() {
        broadcastReceiver?.let {
            application.unregisterReceiver(broadcastReceiver)
        }
    }

    private fun onBroadCastReceiver(): BroadcastReceiver {
        return object : BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {
                val extras = intent?.extras
                val info = extras?.getParcelable<NetworkInfo>("networkInfo")
                value = info?.state == NetworkInfo.State.CONNECTED

            }
        }
    }

}