package com.marcosgribel.indoorway_challenge.core.model.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.marcosgribel.indoorway_challenge.core.model.domain.entity.Data

/**
 * Created by marcosgribel on 7/6/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
@Database(
        exportSchema = false,
        version = 1,
        entities = [
            Data::class
        ]
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun dataDao() : DataDao
}