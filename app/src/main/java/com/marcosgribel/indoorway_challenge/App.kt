package com.marcosgribel.indoorway_challenge

import android.app.Application
import com.marcosgribel.indoorway_challenge.core.di.appModule
import org.koin.android.ext.android.startKoin

/**
 * Created by marcosgribel on 6/29/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
open class App : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin(this, appModule)
    }
}