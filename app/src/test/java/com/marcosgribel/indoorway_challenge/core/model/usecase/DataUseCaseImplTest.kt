package com.marcosgribel.indoorway_challenge.core.model.usecase

import com.marcosgribel.indoorway_challenge.core.model.domain.entity.Data
import com.marcosgribel.indoorway_challenge.core.model.service.DataRepository
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito

/**
 * Created by marcosgribel on 7/8/18.
 *
 *
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 */
class DataUseCaseImplTest {

    lateinit var repositoryMock: DataRepository

    @Before
    fun setUp() {
        repositoryMock = Mockito.mock(DataRepository::class.java)

    }


    @Test
    fun test_getDataFromUrl() {
        val data = Data("https://www.indoorway.com/", "Indoorway")

        Mockito.`when`(repositoryMock.getData(Mockito.anyInt(), Mockito.anyString())).thenReturn(Single.just(data))

        repositoryMock.getData(Mockito.anyInt(), Mockito.anyString()).test()
                .assertValue {
                    it.equals(data)
                }

    }

}