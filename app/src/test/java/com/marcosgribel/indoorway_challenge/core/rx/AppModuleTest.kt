package com.marcosgribel.indoorway_challenge.core.rx

import com.marcosgribel.indoorway_challenge.core.model.domain.entity.Data
import com.marcosgribel.indoorway_challenge.core.model.service.DataRepository
import com.marcosgribel.indoorway_challenge.core.model.usecase.DataUseCase
import com.marcosgribel.indoorway_challenge.core.model.usecase.DataUseCaseImpl
import com.marcosgribel.indoorway_challenge.data.viewmodel.DataViewModel
import io.reactivex.Completable
import io.reactivex.Single
import org.koin.android.architecture.ext.viewModel
import org.koin.dsl.module.applicationContext
import org.mockito.ArgumentMatchers.anyInt
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mockito
import org.mockito.Mockito.`when`

/**
 * Created by marcosgribel on 7/6/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */


val dataModule = applicationContext {

    val fakeDataIndoorway = Data("https://www.indoorway.com/", "Indoorway")
    val repositoryMock = Mockito.mock(DataRepository::class.java)

    `when`(repositoryMock.getData(anyInt(), anyString())).thenReturn(Single.just(fakeDataIndoorway))
    `when`(repositoryMock.insert(fakeDataIndoorway)).thenReturn(Completable.complete())

    bean { repositoryMock }
    bean { DataUseCaseImpl(get()) as DataUseCase }
    viewModel { DataViewModel(get(), get()) }

}

val rxModuleTest = applicationContext {
    bean { AppSchedulerTest() as ScheduleProvider }
}


val appModuleTest = listOf(rxModuleTest, dataModule)