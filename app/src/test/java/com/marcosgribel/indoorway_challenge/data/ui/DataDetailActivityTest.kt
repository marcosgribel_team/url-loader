package com.marcosgribel.indoorway_challenge.data.ui

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.content.Intent
import android.view.View
import com.marcosgribel.indoorway_challenge.R
import com.marcosgribel.indoorway_challenge.core.model.domain.entity.Data
import com.marcosgribel.indoorway_challenge.core.rx.appModuleTest
import com.marcosgribel.indoorway_challenge.data.viewmodel.DataViewModel
import kotlinx.android.synthetic.main.activity_data_content.*
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.standalone.StandAloneContext.closeKoin
import org.koin.standalone.StandAloneContext.startKoin
import org.koin.standalone.inject
import org.koin.test.KoinTest
import org.mockito.Mockito
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment

/**
 * Created by marcosgribel on 7/8/18.
 *
 *
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 */
@RunWith(RobolectricTestRunner::class)
class DataDetailActivityTest : KoinTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()


    private val dataViewModel: DataViewModel by inject()

    private lateinit var dataDetailActivity: DataDetailActivity


    @Before
    fun setUp() {
        var intent = Intent(RuntimeEnvironment.application, DataDetailActivity::class.java)
        intent.putExtra(RuntimeEnvironment.application.getString(R.string.args_url), Mockito.anyString())

        dataDetailActivity = Robolectric.buildActivity(DataDetailActivity::class.java, intent).create().get()

        startKoin(appModuleTest)

    }

    @After
    fun setDown() {
        closeKoin()
    }


    fun test_setData() {
        val data = Data("https://www.indoorway.com/", "Indoorway")
        dataDetailActivity.setData(data)
        assertEquals(data.content, dataDetailActivity.txt_data_detail.text.toString())
    }

    @Test
    fun test_handleError() {
        dataDetailActivity.handleError()

        assertEquals(View.GONE, dataDetailActivity.progress_bar.visibility)
        assertEquals(View.GONE, dataDetailActivity.frame_data_detail.visibility)
        assertEquals(View.VISIBLE, dataDetailActivity.frame_error_404.visibility)
    }

    @Test
    fun test_handleLoading() {
        dataDetailActivity.handleLoading()

        assertEquals(View.VISIBLE, dataDetailActivity.progress_bar.visibility)
        assertEquals(View.GONE, dataDetailActivity.frame_data_detail.visibility)
        assertEquals(View.GONE, dataDetailActivity.frame_error_404.visibility)
    }

    @Test
    fun test_handleSucces() {
        dataDetailActivity.handleSuccess()

        assertEquals(View.GONE, dataDetailActivity.progress_bar.visibility)
        assertEquals(View.VISIBLE, dataDetailActivity.frame_data_detail.visibility)
        assertEquals(View.GONE, dataDetailActivity.frame_error_404.visibility)
    }
}