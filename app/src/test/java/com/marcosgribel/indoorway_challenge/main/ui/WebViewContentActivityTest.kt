package com.marcosgribel.indoorway_challenge.main.ui

import android.content.Intent
import android.view.View
import android.webkit.WebSettings
import com.marcosgribel.indoorway_challenge.R
import com.marcosgribel.indoorway_challenge.webview.ui.WebViewContentActivity
import junit.framework.Assert
import kotlinx.android.synthetic.main.activity_web_content.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment
import org.robolectric.shadows.ShadowToast

/**
 * Created by marcosgribel on 6/30/18.
 *
 *
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 */
@RunWith(RobolectricTestRunner::class)
class WebViewContentActivityTest {

    private lateinit var webContentActivity: WebViewContentActivity

    @Before
    fun setUp(){

        var intent = Intent(RuntimeEnvironment.application, WebViewContentActivity::class.java)
        intent.putExtra(RuntimeEnvironment.application.getString(R.string.args_url), Mockito.anyString())

        webContentActivity = Robolectric.buildActivity(WebViewContentActivity::class.java, intent).create().get()
    }


    @Test
    fun assertOnOnLine(){
        webContentActivity.onOnline()
        Assert.assertEquals(WebSettings.LOAD_DEFAULT, webContentActivity.web_view.settings.cacheMode)
    }

    @Test
    fun assertOnOffline(){

        webContentActivity.onOffline()
        Assert.assertEquals(WebSettings.LOAD_CACHE_ELSE_NETWORK, webContentActivity.web_view.settings.cacheMode)

        val textShowed = ShadowToast.getTextOfLatestToast()
        org.junit.Assert.assertEquals("You\'re offline.", textShowed)

    }

    @Test
    fun assertOnLoadSuccess(){
        webContentActivity.onLoadContentSuccess()
        Assert.assertEquals(View.VISIBLE, webContentActivity.web_view.visibility)
        Assert.assertEquals(View.GONE, webContentActivity.progress_bar.visibility)

    }
}