package com.marcosgribel.indoorway_challenge.main.ui

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.widget.Button
import com.marcosgribel.indoorway_challenge.R
import com.marcosgribel.indoorway_challenge.data.ui.DataDetailActivity
import com.marcosgribel.indoorway_challenge.webview.ui.WebViewContentActivity

import org.hamcrest.Matchers
import org.junit.Assert.assertEquals
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.test.KoinTest
import org.robolectric.Robolectric

import org.robolectric.RobolectricTestRunner
import org.robolectric.Shadows.shadowOf
import org.robolectric.shadows.ShadowToast

/**
 * Created by marcosgribel on 6/29/18.
 *
 *
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 */
@RunWith(RobolectricTestRunner::class)
class MainActivityTest : KoinTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    private lateinit var mainActivity: MainActivity

    @Before
    fun setUp(){
        mainActivity = Robolectric.setupActivity(MainActivity::class.java)
    }


    @Test
    fun assertOfflineMessage(){
        mainActivity.onOffline()

        val textShowed = ShadowToast.getTextOfLatestToast()
        assertEquals("You\'re offline.", textShowed)

    }

    @Test
    fun test_OpenWebContentActivity(){
        val button = mainActivity.findViewById<Button>(R.id.btn_download_with_webview)
        button.performClick()

        val shadowActivity = shadowOf(mainActivity)
        val startedIntent = shadowActivity.nextStartedActivity
        val shadowIntent = shadowOf(startedIntent)
        assertThat(shadowIntent.intentClass.name, Matchers.equalTo(WebViewContentActivity::class.java.name))
    }

    @Test
    fun test_onDownloadWithoutWebViewClickListener(){

        val button = mainActivity.findViewById<Button>(R.id.btn_download_without_webview)
        button.performClick()

        val shadowActivity = shadowOf(mainActivity)
        val startedIntent = shadowActivity.nextStartedActivity
        val shadowIntent = shadowOf(startedIntent)
        assertThat(shadowIntent.intentClass.name, Matchers.equalTo(DataDetailActivity::class.java.name))

    }

}

