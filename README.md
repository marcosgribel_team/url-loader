# README #


### Get Started ###

* [Android Studio](https://developer.android.com/studio/index.html)
* [Java 1.8](http://www.oracle.com/technetwork/pt/java/javase/downloads/jdk8-downloads-2133151.html)
* [Kotlin](https://kotlinlang.org/)
* [Configure Kotlin plugin on Android Studio](https://blog.jetbrains.com/kotlin/2013/08/working-with-kotlin-in-android-studio/)

#### Description ####

This project leverages on modern approaches to build Android applications, with architecture is based on [MVVM](https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93viewmodel) and [Clean Architecture](https://8thlight.com/blog/uncle-bob/2012/08/13/the-clean-architecture.html) and powered by [Android Architecture Components](https://developer.android.com/topic/libraries/architecture/room).

- 100% written in Kotlin programming language.
- Including test at unit and integration test.
- Solid Reactive behavior with LiveData.

### Challenge ###

##### - Task
   - Implement Android application using Kotlin or Java with any frameworks, libraries you’re familiar with. 
Please pay attention to application architecture, testability, proper use of Android components and network service.

#####- Requirements
   - User story: Simple Activity with text field (for address input) and button "download". 
   On button click, source of given url should be displayed.


#### Libraries ####
- Koin (DI)
- Architecture Components
- Timber
- Robolectric
- Mockito